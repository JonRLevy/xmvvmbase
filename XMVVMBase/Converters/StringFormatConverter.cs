﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace JonRLevy.XMVVMBase.Converters
{
    /// <summary>
    /// The StringFormatConverter receives an object, and formats it using the String.Format method - using the FormatString
    /// supplied as parameter, or - if not supplied - using the FormatsString property. (If neither a parameter or 
    /// FormatString-property is supplied, the ToString method of the value is used.
    /// </summary>
    public class StringFormatConverter : IValueConverter
    {
        /// <summary>
        /// This property is used to format the supplied value in a call to String.Format; unless overridden
        /// by a parameter in the Convert-call.
        /// </summary>
        public string FormatString { get; set; } = null;

        /// <summary>
        /// The Convert method receives a value-object, and formats it using the String.Format method - using the FormatString
        /// supplied as parameter, or - if not supplied - using the FormatsString property. (If neither a parameter or 
        /// FormatString-property is supplied, the ToString method of the value is used.
        /// </summary>
        /// <param name="value">The object to format</param>
        /// <param name="targetType">Not used - will be a string</param>
        /// <param name="parameter">A string used in a call to String.Format. If not supplied the FormatString-property
        /// is used instead.</param>
        /// <param name="cultureInfo">Not used.</param>
        /// <returns>The string resulting from using the supplied value as parameter in a call to String.Format using
        /// the supplied parameter/FormatString.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            string formatString = parameter != null ? parameter.ToString() : FormatString;

            return formatString == null ? value.ToString() : String.Format(formatString, value);
        }

        /// <summary>
        /// Not impelemented
        /// </summary>
        /// <param name="value">Not used</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Not used</param>
        /// <param name="cultureInfo">Not used</param>
        /// <returns>None, will throw a NotImplemented-exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }
    }
}
