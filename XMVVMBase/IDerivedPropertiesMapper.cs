﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.XMVVMBase
{
    /// <summary>
    /// An interface for getting an enumeration of properties derived from the baseProperty (including the
    /// baseProperty itself).
    /// </summary>
    public interface IDerivedPropertiesMapper
    {
        /// <summary>
        /// Supplied with the name of a baseProperty, an implementation must supply an enumeration of property names,
        /// derived from the base property - including the name of the base property itself; the purpose being to be able to
        /// notify changes to both the base property and derived properties.
        /// </summary>
        /// <param name="baseProperty">The name of the base property derived properties should be derived from.</param>
        /// <returns>An enumeration of names of properties derived from the baseProperty, including the baseProperty.</returns>
        IEnumerable<string> GetDerivedProperties(string baseProperty);
    }
}
