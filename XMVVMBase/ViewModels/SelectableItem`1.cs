﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.XMVVMBase.ViewModels
{
    /// <summary>
    /// An generic class meant for members of lists for display; providing access to the type T, as the Model, and adding 
    /// a property that tells if the item is selected or not (the setting of which must be handled externally).
    /// </summary>
    /// <typeparam name="T">The encapsulated class, most probably a Model Object.</typeparam>
    public class SelectableItem<T> : BindableBase
    {
        /// <summary>
        /// The encapsulated model object.
        /// </summary>
        protected T model;
        /// <summary>
        /// The property encapsulating the Model-object.
        /// </summary>
        public T Model
        {
            get => model;
            set => SetProperty(ref model, value);
        }

        /// <summary>
        /// Whether this item is selected or not.
        /// </summary>
        protected bool isSelected;

        /// <summary>
        /// This property should be set to true or false, depending on if the item should be shown as selected or not.
        /// </summary>
        public virtual bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value);
        }

        /*
         *      Access property in Model as
         *      public int Whatever
         *      {
         *          get => Model.Whatever;
         *          set
         *          {
         *              if (value!=Model.Whatever)
         *              {
         *                  Model.Whatever=value;
         *                  OnPropertyChanged();
         *              }
         *          }
         *      }
        */
    }
}