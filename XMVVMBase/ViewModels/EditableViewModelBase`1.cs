﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace JonRLevy.XMVVMBase.ViewModels
{
    /// <summary>
    /// The EditableViewModelBase adds basic properties and methods to determine if a ViewModel is in editmode or not.
    /// </summary>
    /// <typeparam name="TApp">The Application class the ViewModel is used in.</typeparam>
    public class EditableViewModelBase<TApp> : ViewModelBase<TApp>
        where TApp : Application
    {

        /// <summary>
        /// Creates a EditableViewModelBase, calls registerDerivedPropertiesAndDependentCommands to initialize'
        /// it's propertiesAndCommandsMap.
        /// </summary>
        public EditableViewModelBase()
        {
            registerDerivedPropertiesAndDependentCommands();
        }

        /// <summary>
        /// A <see cref="DerivedPropertiesAndDependentCommandsMapper"/> to keep track of which properties and commands
        /// depend on a property.
        /// </summary>
        protected DerivedPropertiesAndDependentCommandsMapper propertiesAndCommandsMap = new DerivedPropertiesAndDependentCommandsMapper();

        /// <summary>
        /// Initializes dependencies. Override to add own.
        /// </summary>
        protected virtual void registerDerivedPropertiesAndDependentCommands()
        {
            propertiesAndCommandsMap.AddDependentCommand(GotoEditMode_Command as Command, nameof(IsInEditMode));

            propertiesAndCommandsMap.AddDependentCommand(SaveEdits_Command as Command, nameof(IsInEditMode));
            propertiesAndCommandsMap.AddDependentCommand(SaveEdits_Command as Command, nameof(IsEdited));
        }

        private bool isInEditMode;

        /// <summary>
        /// Determines if the ViewModel is in EditMode
        /// </summary>
        public bool IsInEditMode
        {
            get => isInEditMode;
            set => SetProperty(ref isInEditMode, value);
        }

        /// <summary>
        /// Override to add own logic whether EditMode can be entered. In this baseversion it returns true if
        /// not already in EditMode.
        /// </summary>
        /// <returns>True, if not currently in EditMode.</returns>
        virtual public bool DetermineCanEnterEditMode()
            => !IsInEditMode;

        /// <summary>
        /// Virtual method to enter EditMode
        /// </summary>
        virtual public void GoToEditMode()
            => IsInEditMode = true;

        private Command gotoEditMode_Command;
        /// <summary>
        /// Command to enter EditMode; calls <see cref="GoToEditMode"/>.
        /// </summary>
        public ICommand GotoEditMode_Command
            => gotoEditMode_Command ?? (gotoEditMode_Command = new Command(GoToEditMode, DetermineCanEnterEditMode));

        private bool isEdited;
        /// <summary>
        /// Property to keep track of whether the data for a ViewModel has been edited / "is dirty".
        /// </summary>
        public bool IsEdited
        {
            get => isEdited;
            set => SetProperty(ref isEdited, value);
        }

        /// <summary>
        /// Virtual method to determine if a ViewModel is valid, i.e can be saved. Override to add business logic.
        /// </summary>
        /// <returns>True.</returns>
        public virtual bool DetermineIsValid()
            => true;

        /// <summary>
        /// Virtual method to determine if the data for a ViewModel can be saved. In the base version returns true if
        /// the ViewModel is in EditMode, IsEdited and is valid.
        /// </summary>
        /// <returns></returns>
        public virtual bool DetermineCanSaveEdits()
            => (IsInEditMode && IsEdited && DetermineIsValid());

        /// <summary>
        /// Virtual method to save the data for a ViewModel. In the base version it simply sets <see cref="IsEdited"/> and <see cref="IsInEditMode"/> to false.
        /// </summary>
        public virtual void SaveEdits()
        {
            IsEdited = false;
            IsInEditMode = false;
        }

        private Command saveEdits_Command;

        /// <summary>
        /// Command to save edits to the data in a ViewModel. Calls <see cref="SaveEdits"/>
        /// </summary>
        public ICommand SaveEdits_Command
            => saveEdits_Command ?? (saveEdits_Command = new Command(SaveEdits, DetermineCanSaveEdits));

        /// <summary>
        /// Virtual method to AbortEdits. In the base version it simply sets <see cref="IsEdited"/> and <see cref="IsInEditMode"/> to false.
        /// </summary>
        public virtual void AbortEdits()
        {
            IsEdited = false;
            IsInEditMode = false;
        }

        private Command abortEdits_Command;

        /// <summary>
        /// Command to abort edits, call the <see cref="AbortEdits"/> method.
        /// </summary>
        public ICommand AbortEdits_Command
            => abortEdits_Command ?? (abortEdits_Command = new Command(AbortEdits));
    }
}
