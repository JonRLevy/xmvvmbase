﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JonRLevy.XMVVMBase.ViewModels
{
    /// <summary>
    /// A generic VievModelBase, with a Model of type T
    /// </summary>
    /// <typeparam name="TApp">The Application class the ViewModel is used in.</typeparam>
    /// <typeparam name="TModel">The Type of the conatined model</typeparam>
    public class ViewModelBase<TApp, TModel> : ViewModelBase<TApp>
        where TApp : Application
    {
        /// <summary>
        /// The encapsulated model object.
        /// </summary>
        protected TModel model;
        /// <summary>
        /// The property encapsulating the Model-object.
        /// </summary>
        public TModel Model
        {
            get => model;
            set => SetProperty(ref model, value);
        }

        /// <summary>
        /// Constructor that sets the Model-object
        /// </summary>
        /// <param name="model">The model object to be encapsulated.</param>
        public ViewModelBase(TModel model)
        {
            this.model = model;
        }
    }
}
