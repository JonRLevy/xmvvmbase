﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JonRLevy.XMVVMBase.ViewModels
{
    /// <summary>
    /// An abstract base class for ViewModels, containing utilities shared, not included in <see cref="BindableBase"/> to keep that lightweight.
    /// </summary>
    /// <typeparam name="TApp">The Application class the ViewModel is used in.</typeparam>
    public abstract class ViewModelBase<TApp> : BindableBase
        where TApp : Application
    {
        /// <summary>
        /// Access to the current Application object, as a TApp.
        /// </summary>
        public TApp App
        {
            get => Application.Current as TApp;
        }

        /// <summary>
        /// A simple read-only property, saying if the ViewModel is in DesignMode.
        /// </summary>
        public bool IsInDesignMode
        {
            //            get => Windows.ApplicationModel.DesignMode.DesignModeEnabled;
            get => false; //TODO
        }
    }
}
