﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JonRLevy.XMVVMBase
{
    /// <summary>
    /// An interface for getting an enumeration of <see cref="RelayCommandBase"/> whose ability to be executed
    /// is dependent on the baseProperty.
    /// </summary>
    public interface IDependentCommandsMapper
    {
        /// <summary>
        /// Supplied with the name of a propertyName, an implementation should return an enumeration of <see cref="RelayCommandBase"/> that has an
        /// CanExecute depending on the value of the baseProperty; the intent being to be able to raise the CanExecuteChanged event for these commands. 
        /// </summary>
        /// <param name="propertyName">The name of the property dependent commands should be returned for.</param>
        /// <returns>An enumeration of <see cref="RelayCommandBase"/> whose ability to be executed depends on the value of the supplied property.</returns>
        IEnumerable<Command> GetDependentCommands(string propertyName);
    }
}
